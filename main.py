# The controller of the MVC Pattern which controls the communication between View and Model
import builtins
import sys
sys.path.append("/sd")
from settings import *

if CPYTHON:
    import os
    sys.path.append(os.path.join(os.path.dirname(__file__), 'mplib'))
else:
    sys.path.append("/sd/mplib")
import mpywrapper as mp

mp.init()
v = mp.view.View()
m = mp.model.Model()
b1 = mp.button.Button(10,10,200,100)

# Define Widget Objects

# Widget has recognized an input event, apply changes to model
# TODO: apply pressed state of button to the widget like " b1.pressed=true; b1.setUpdate() "
#       to give visual feedback (consider: possibly not each button interaction changes model, but visual feedback is helpful)
def b1TCallback(x,y,evt):
    m.increment()

# Model has changed, apply changes to widget and set update flag
def b1MCallback(counter):
    b1.settext(str(counter))
    b1.setUpdate()

# Register callbacks to the widget
b1.TCallbackregister(b1TCallback)
b1.MCallbackregister(b1MCallback)


# Register all Widget callbacks to the controller callback
def MCallback(counter):
#    print("MCallback")
    for el in v.nodes:
        for f in el.MCallbackList:
            f(counter)
    v.setUpdateFlag()


def TCallback(x, y, evt):
    for el in v.nodes:
#        print(el)
        for f in el.TCallbackList:
            if (el.containsPt(x,y) and evt == EVENT_RELEASE):
                f(x,y,evt)


# Register controller callback handlers to the input event streams (Touch and Model)
mp.irq.registerTouchCallback(TCallback)
mp.irq.enableAll()
m.MCallbackregister(MCallback)
# Init the view tree
v.addViewObj(b1)
v.setUpdateFlag()

# Start scheduler
loop = mp.uasyncio.get_event_loop()
loop.run_forever()

print("Program finished")
