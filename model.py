# The model is representing the data of the application backend

print("importing model")

class Model:
    def __init__(self):
        self.AppState = 'A'
        self.counter = 1
        self.MCallbackList = []

    def increment(self):
        self.counter += 1
        self.notify()

    def setCounter(self, val):
        self.counter = val
        self.notify()

    def notify(self):
        print("model: notify")
        for element in self.MCallbackList:
            element(self.counter)

    def MCallbackregister(self, fun):
        self.MCallbackList.append(fun)
        print("registered to Model MCallbacklist, List content: " + str(self.MCallbackList))