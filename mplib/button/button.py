# Button widget (input/output)
import mpywrapper as mp

class Button:
    def __init__(self, x, y, w, h):
        self.x =  x
        self.y = y
        self.w = w
        self.h = h
        self.updateFlag = False
        self.text = ""
        self.MCallbackList = []
        self.TCallbackList = []
        self.draw()

    # register functions of the Widget which react to model changes
    def MCallbackregister(self, fun):
        self.MCallbackList.append(fun)
        print("registered to Button MCallback, List content: " + str(self.TCallbackList))

    # register functions of the Widget which react to Touch input
    def TCallbackregister(self, fun):
        self.TCallbackList.append(fun)
        print("registered to Button TCallback, List content: "+str(self.TCallbackList))

    def containsPt(self, x,y):
        if (x > self.x and x < self.x + self.w and y > self.y and y < self.y + self.h):
            return True
        return False

    def setUpdate(self):
        self.updateFlag = True

    def getUpdateStatus(self):
        if (self.updateFlag == True):
            return True
        return False

    def resetUpdateStatus(self):
        self.updateFlag = False

    def settext(self, str):
        self.text = str;

    def draw(self):
        print("button: draw with text:",self.text )
        mp.gfx.set_color(mp.color_convert("0xC0C0C0"))
        mp.gfx.draw_rectangle(10,10,100,100)