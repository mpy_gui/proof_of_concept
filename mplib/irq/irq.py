from settings import *
import mpywrapper as mp

# print("importing irq")

loop = mp.uasyncio.get_event_loop()


class IRQ():
    def registerTouchCallback(self, fun):
        # print("irq registerTouchCallback")
        mp.io.registerTouchCallback(fun)

    def enableAll(self):
        mp.io.enableTouch()
        loop = mp.uasyncio.get_event_loop()
        loop.create_task(self.irqPoll())

    async def irqPoll(self):
        # print("irqPoll started")
        while True:
            mp.io.execTouchCallbacks()
            if (CPYTHON == True):
                await mp.uasyncio.sleep(1 / 10)
            else:
                await mp.uasyncio.sleep_ms(50)


irqObj = IRQ()


def registerTouchCallback(fun):
    irqObj.registerTouchCallback(fun)


def enableAll():
    irqObj.enableAll()
