# Simulation of touch interface
print("import mpio")
import mpywrapper as mp
import random
import time
from mpywrapper import debugprint

class Touch:


    def __init__(self):
        self.TCallbackList = []
        self.EventList  = []
        self.tthr = None
        mp.gfxObj.bind('<Motion>', self.mouseMove)
        mp.gfxObj.bind("<Button-1>", self.mousePress)
        mp.gfxObj.bind("<ButtonRelease-1>", self.mouseRelease)

    def registerTouchCallback(self, fun):
        self.TCallbackList.append(fun)
        debugprint("registered to IO TcallbackList, List content: "+ str(self.TCallbackList))

    def mouseMove(self, event):
        EVENT_MOVE = 1
        message = event.x, event.y, EVENT_MOVE
        self.EventList.insert(0,message)

    def mousePress(self, event):
        EVENT_PRESS = 2
        message = event.x, event.y, EVENT_PRESS
        self.EventList.insert(0, message)

    def mouseRelease(self, event):
        EVENT_RELEASE = 4
        message = event.x, event.y, EVENT_RELEASE
        self.EventList.insert(0, message)



#### this part has to be replaced by real touch input
    '''
    async def simTouchEvents(self):
        while (True):
            print("SIM TOUCH EVENT")
            # x, y, evt
            message = (random.randint (0,480), random.randint (0,272), random.randint (0,3))
            print("Touch input x: "+str(message[0])+" y: "+str(message[1])+" evt: "+str(message[2]))
            self.EventList.insert(0,message)
            await mp.uasyncio.sleep(0.5)

    def startTouchSimulation(self):
        loop = mp.uasyncio.get_event_loop()
        loop.create_task(self.simTouchEvents())
    '''

## triggered by poll event loop
    def execTouchCallbacks(self):
        # for message in self.EventList:
        while (len(self.EventList) > 0):
            message = self.EventList.pop()
            for fun in self.TCallbackList:
                fun(message[0], message[1], message[2])

tObj = Touch()


def registerTouchCallback(fun):
    tObj.registerTouchCallback(fun)

def enableTouch():
    pass
    #tObj.startTouchSimulation()

def execTouchCallbacks():
    tObj.execTouchCallbacks()