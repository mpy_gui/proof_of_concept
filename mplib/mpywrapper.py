import sys
from settings import *


def init():
    if (CPYTHON == True):
        print("Init for use with pycharm...")
        import os
        dirpath = os.path.dirname(__file__)
        sys.path.append(os.path.join(dirpath, 'uerrno'))
        sys.path.append(os.path.join(dirpath, 'irq'))
        sys.path.append(os.path.join(dirpath, 'gfx'))
        sys.path.append(os.path.join(dirpath, 'mpio'))
        sys.path.append(os.path.join(dirpath, 'button'))
        global uasyncio
        global irq
        global button
        global canvas
        global textdisplay
        global view
        global model
        global uerrno
        global gfx
        global io
        global gfxObj
        global writer
        import uerrno
        import asyncio as uasyncio
        import view
        import irq
        import gfx
        import mpio as io
        import button
        import model
    else:
        print("Init for use on hardware...")
        sys.path.clear()
        sys.path.append("/sd")
        sys.path.append("/sd/mplib")
        sys.path.append("/sd/mplib/uerrno")
        sys.path.append("/sd/mplib/view")
        sys.path.append("/sd/mplib/uasyncio")
        sys.path.append("/sd/mplib/irq")
        sys.path.append("/sd/mplib/button")
        global uasyncio
        global irq
        global button
        global view
        global model
        global uerrno
        global gfx
        global io
        import uerrno
        import uasyncio
        import view
        import irq
        import gfx
        import io
        import button
        import model

def color_convert(hex_rrggbb):
    if (CPYTHON == True):
        ret = str(hex_rrggbb)
        ret = ret[2:]
        ret = "#"+ret
        return ret
    else:
        ret = str(hex_rrggbb)
        ret = ret[2:]
        return int(ret,16)

def debugprint(line):
    if DEBUGPRINT == True:
        print(line)