# Class to implement the View Logic and to render the view tree
from settings import *
if (CPYTHON == True):
    import asyncio as uasyncio
else:
    import uasyncio

print("importing view")

class View:
    def __init__(self):
        self.updateFlag = False
        self.nodes = []
        self.vthr = None
        global uasyncio
        loop = uasyncio.get_event_loop()
        loop.create_task(self.displayTree())

    def addViewObj(self, obj):
        self.nodes.append(obj)

    def setUpdateFlag(self):
        self.updateFlag = True

    async def displayTree(self):
        while (True):
            if (self.updateFlag == True):
                self.updateFlag = False
                for obj in self.nodes:
                    if (obj.getUpdateStatus() == True):
                        print("view: redrawing object...")
                        obj.resetUpdateStatus()
                        obj.draw()
            if CPYTHON == True:
                await uasyncio.sleep(1/10)
                pass
            else:
                await uasyncio.sleep_ms(200)
